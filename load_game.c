#include <stdlib.h>

#include "load_game.h"
#include "from_main_to_game_data.h"
#include "game_window.h"
#include "game_back_end.h"

#define MAKS_DL_TEKSTU 100

BoardData *convert_MGData_to_BoardData( MGData settings ){
    BoardData *data = malloc(sizeof(struct BoardData_type));
    if (data == NULL){
        fprintf(stderr,"Brak pamieci game_window.c/create_game_window/data\n");
        exit(1);
    }
    data->load=1;

    data->mode=settings->mode;

    if(settings->board_size == 1){
        data->size_x = 5;
        data->size_y = 4;
    }
    if(settings->board_size == 2){
        data->size_x = 8;
        data->size_y = 5;
    }
    printf("board_size: %d\nsize_x: %d\tsize_y: %d\n",settings->board_size, data->size_x, data->size_y);
    data->size_full = data->size_x*data->size_y;
    data->size_half = (data->size_x*data->size_y) / 2;

    data->icon_set=settings->icon_set;

    //data->cards_left = 1;
    data->cards_left = data->size_half; //TESTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT

    data->quit = false;

    for(int i=0; i<data->size_full; i++){
        data->taken[i]=false;
    }

    data->clicked[0] = -1;
    data->clicked[1] = -1;

    data->score[0] = 0;
    data->score[1] = 0;

    data->quit = false;
    data->game_end_window = NULL;

    return data;
}


gboolean communication( gpointer data_temp ){
    printf("communication\n");
    BoardData *data = data_temp;
    if(data->load == 1){
        if( data->mode == 'A' || data->mode == 'B' ){
            data->channel = initPipes( data->mode);
            printf( "Rozpoczęto synchronizacje\n" );
            gchar wyjscie[MAKS_DL_TEKSTU];
            sprintf(wyjscie, "R");
            sendStringToPipe( data->channel, wyjscie );
            printf( "Wysłano: %s\n", wyjscie );
        }
        printf("communication END\n\n");
        data->load++;
        return FALSE;
    }
    else
        return TRUE;
}

gboolean synchronize( gpointer data_temp ){
    printf("synchronize\n\n");
    BoardData *data = data_temp;
    if(data->load == 2 && ( data->mode == 'A' || data->mode == 'B' ) ){
        gchar wejscie[MAKS_DL_TEKSTU];
        gchar wyjscie[MAKS_DL_TEKSTU];
        if( getStringFromPipe( data->channel, wejscie, MAKS_DL_TEKSTU ) ){
            printf("Odebrano: %s\n", wejscie);
            if(wejscie[0]=='R' && wejscie[1]=='\0'){
                sprintf(wyjscie, "S");
                sendStringToPipe( data->channel, wyjscie );
                printf("Wysłano: %s\n", wyjscie);
                printf("Zakonczono synchronizacje\n\n");
            }
            if(wejscie[0]=='S' && wejscie[1]=='\0'){
                printf("Zakonczono synchronizacje\n\n");
            }
            data->load++;
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    if(data->load == 2 && data->mode == 'S' ){
        data->load++;
        return FALSE;
    }
    return TRUE;
}

gboolean create_board( gpointer data_temp ){
    printf("create_board\n\n");
    BoardData *data = data_temp;
    if(data->load == 3){
        if( data->mode == 'A' ){
                if( get_board( data )){
                    board_print(data);
                    data->turn = false;
                    data->load++;
                    return FALSE;
                }
                else{
                    return TRUE;
                }
        }
        if( data->mode == 'B' ){
            board_generator( data->size_full, data->tab );
            send_board( data );
            data->turn = true;
        }
        if( data->mode == 'S' )
            board_generator( data->size_full, data->tab );
        board_print(data);
        data->load++;
        return FALSE;
    }
    else
        return TRUE;
}

gboolean start_game( gpointer data_temp ){
    printf("start_game\n\n");
    BoardData *data = data_temp;
    if(data->load == 4){
        create_game_window(data);
        data->load++;
        return FALSE;
    }
    else
        return TRUE;
}

void load_game( GtkWidget* widget, gpointer data_from_main ){
    BoardData *data = convert_MGData_to_BoardData( data_from_main );
    g_timeout_add(100, communication, data);
    g_timeout_add(100, synchronize, data);
    g_timeout_add(100, create_board, data);
    g_timeout_add(100, start_game, data);
}
