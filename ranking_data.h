

typedef struct RankingData_type{
    int single_number;
    char single_name[5][20];
    int single_score[5];
    int multi_number;
    char multi_name[5][20];
    int multi_score[5];
}nRankingData;
