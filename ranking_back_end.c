#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ranking_back_end.h"
#include "ranking_data.h"


RankingData create_RankingData_struct (void){
    RankingData data;
    data = malloc (sizeof (struct RankingData_type) );
    if (data == NULL){
        fprintf (stderr, "Brak pamieci ranking_back_end.c/create_RankingData_struct/data( struct RankingData_type)\n");
        exit(1);
    }
    data->single_number = 0;
    data->multi_number = 0;
    return data;
}

void printf_ranking (RankingData data){
    printf("single_number %d\n", data->single_number);
    for( int i=0; i < 5; i++){
        printf("%d\t", i);
        printf("%s\t", data->single_name[i]);
        printf("%d\n", data->single_score[i]);
    }

    printf("multi_number %d\n", data->multi_number);
    for( int i=0; i<5; i++){
        printf("%d\t", i);
        printf("%s\t", data->multi_name[i]);
        printf("%d\n", data->multi_score[i]);
    }

    printf("\n");
}

bool read_ranking (RankingData data){
    FILE *fp;

    fp = fopen("ranking", "rb");

    if (fp == NULL){
        printf("Nie mozna otworzyc pliku ranking do odczytu\n\n");
        return false;
    }
    else{
        printf("Otwarto plik ranking do odczytu\n");

        fread(&data->single_number, sizeof(data->single_number), 1, fp);
        for( int i=0; i < 5; i++){
            fread(&data->single_name[i], sizeof(data->single_name[i]), 1, fp);
            fread(&data->single_score[i], sizeof(data->single_score[i]), 1, fp);
        }
        fread(&data->multi_number, sizeof(data->multi_number), 1, fp);
        for( int i=0; i<5; i++){
            fread(&data->multi_name[i], sizeof(data->multi_name[i]), 1, fp);
            fread(&data->multi_score[i], sizeof(data->multi_score[i]), 1, fp);
        }

        printf_ranking (data);
        fclose (fp);
        return true;
    }
}

void save_ranking(RankingData data){
    FILE *fp;

    fp = fopen("ranking", "wb");

    if (fp == NULL){
        printf("Nie mozna otworzyc pliku ranking do zapisu\n\n");
        free(data);
        exit(1);
    }
    else{
        printf("Otwarto plik ranking do zapisu\n");

        fwrite(&data->single_number, sizeof(data->single_number), 1, fp);
        for( int i=0; i < 5; i++){
            fwrite(&data->single_name[i], sizeof(data->single_name[i]), 1, fp);
            fwrite(&data->single_score[i], sizeof(data->single_score[i]), 1, fp);
        }

        fwrite(&data->multi_number, sizeof(data->multi_number), 1, fp);
        for( int i=0; i<5; i++){
            fwrite(&data->multi_name[i], sizeof(data->multi_name[i]), 1, fp);
            fwrite(&data->multi_score[i], sizeof(data->multi_score[i]), 1, fp);
        }
        printf_ranking (data);
        fclose (fp);
        free (data);
        return;
    }
    //Dopisaæ zapisywanie w trybie tekstowym dla zmylenia przeciwnika;
}

int check_ranking_position  (int score, char mode){
    RankingData data = create_RankingData_struct();

    if( !read_ranking(data) ){
        free(data);
        return 0;
    }
    int poz;
    if( mode == 'S' ){
        if( data->single_number == 0 ){
            free(data);
            return 0;
        }
        for( int i=0; i < data->single_number; i++ ){
            if( score < data->single_score[i] ){
                free(data);
                return i;
            }
        }
        if( data->single_number < 5 ){
            poz = data->single_number;
            free(data);
            return poz;
        }
        free(data);
        return -1;
    }

    if( mode == 'A' || mode == 'B' || mode == 'M' ){
        if( data->multi_number == 0 ){
            free( data );
            return 0;
        }
        for( int i=0; i < data->multi_number; i++ ){
            if( score < data->multi_score[i] ){
                free(data);
                return i;
            }
        }
        if( data->multi_number < 5 ){
            poz = data->multi_number;
            free(data);
            return poz;
        }
        free(data);
        return -1;
    }
    free(data);
    return -10;
}

void add_to_ranking(int score, const char name[20], char mode){
    printf("add_to_ranking\n");
    printf("%d\t%s\t%c\n\n", score, name, mode);

    RankingData data = create_RankingData_struct();

    int poz;
    poz = check_ranking_position(score, mode);

    if( !read_ranking(data) ){
        data->single_number = 0;
        data->multi_number = 0;
    }

    if( mode == 'S' ){
        if(data->single_number < 5)
            data->single_number++;
        for( int i = data->single_number-1; i>poz && i>0 ;i--){
            strcpy(data->single_name[i], data->single_name[i-1]);
            data->single_score[i] = data->single_score[i-1];
        }
        strncpy( data->single_name[poz], name, sizeof(data->single_name[poz]) - 1);
        data->single_name[poz][19]='\0';
        data->single_score[poz]=score;
    }

    if( mode == 'A' || mode == 'B' || mode == 'M' ){
        if(data->multi_number < 5)
            data->multi_number++;
        for( int i=data->multi_number-1; i>poz && i>0 ;i--){
            strcpy(data->multi_name[i], data->multi_name[i-1]);
            data->multi_score[i] = data->multi_score[i-1];
        }
        strncpy( data->multi_name[poz], name, sizeof(data->multi_name[poz]) - 1);
        data->multi_name[poz][19]='\0';
        data->multi_score[poz]=score;
    }
    save_ranking(data);
}
