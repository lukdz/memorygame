#include <gtk/gtk.h>
#include <stdlib.h>

#include "main_window.h"
#include "load_game.h"
#include "ranking_window.h"
#include "from_main_to_game_data.h"


typedef struct MainData_type *MainData;

struct MainData_type{
    GtkWindow *window;
    GtkWidget *server_box;
    MGData settings;
};


void hide_box(MainData data){
    gtk_widget_hide(data->server_box);
    int width, height;
    gtk_window_get_size (data->window, &width, &height);
    gtk_window_resize (data->window, width, 1);
}

void show_box(MainData data){
    gtk_widget_show(data->server_box);
}


void clicked_single (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->mode = 'S';
    printf("clicked_single\n");
    printf("Mode: %c\n\n", data_temp->settings->mode);
    show_box(data_temp);
}

void clicked_klient (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->mode = 'A';
    printf("clicked_klient\n");
    printf("Mode: %c\n\n", data_temp->settings->mode);
    hide_box(data_temp);
}

void clicked_serwer (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->mode = 'B';
    printf("clicked_serwer\n");
    printf("Mode: %c\n\n", data_temp->settings->mode);
    show_box(data_temp);
}


void clicked_normal (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->board_size = 1;
    printf("clicked_normal\n");
    printf("board_size: %d\n\n", data_temp->settings->board_size);
    show_box(data_temp);
}

void clicked_hard (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->board_size = 2;
    printf("clicked_hard\n");
    printf("board_size: %d\n\n", data_temp->settings->board_size);
    show_box(data_temp);
}


void clicked_icon_set0 (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->icon_set = 0;
    printf("clicked_icon_set0\n");
    printf("icon_set: %d\n\n", data_temp->settings->icon_set);
}

void clicked_icon_set1 (GtkWidget* widget, gpointer data){
    MainData data_temp = data;
    data_temp->settings->icon_set = 1;
    printf("clicked_icon_set1\n");
    printf("icon_set: %d\n\n", data_temp->settings->icon_set);
}


MainData create_struct_MainData(void){
    MainData data = malloc(sizeof(struct MainData_type));
    if (data == NULL){
        fprintf(stderr,"Brak pamieci main_window.c/create_struct_MainData/data(struct MainData_type)\n");
        exit(1);
    }
    data->settings = malloc(sizeof(struct main_game_type));
    if (data->settings == NULL){
        fprintf(stderr,"Brak pamieci main_window.c/create_struct_MainData/data->settings(struct main_game_type)\n");
        exit(1);
    }
    data->settings->mode = 'S';
    data->settings->board_size = 1;
    data->settings->icon_set = 0;
    return data;
}

void create_main_window( void ){
    MainData data = create_struct_MainData();

    data->window = (GtkWindow*) gtk_window_new(GTK_WINDOW_TOPLEVEL);

    gtk_window_set_title (GTK_WINDOW (data->window), "Gra memory - Menu");
    g_signal_connect (data->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    gtk_container_set_border_width (GTK_CONTAINER (data->window), 10);


    GtkWidget *mbox;
    mbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add (GTK_CONTAINER (data->window), mbox);

    GtkLabel *mode_label;
    mode_label = (GtkLabel*)gtk_label_new("Wybierz gracza");
    gtk_box_pack_start(GTK_BOX(mbox), (GtkWidget*)mode_label, TRUE, TRUE, 0);

    GtkWidget *radioS,*radioMK, *radioMS, *boxSKS;
    boxSKS = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_set_homogeneous (GTK_BOX (boxSKS), TRUE);

    radioS = gtk_radio_button_new_with_label_from_widget (NULL, "Pojedynczy");
    g_signal_connect (radioS, "clicked", G_CALLBACK (clicked_single), data);
    radioMK = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radioS), "Klient");
    g_signal_connect (radioMK, "clicked", G_CALLBACK (clicked_klient), data);
    radioMS = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radioS),"Serwer");
    g_signal_connect (radioMS, "clicked", G_CALLBACK (clicked_serwer), data);


    data->server_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start (GTK_BOX (boxSKS), radioS, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (boxSKS), radioMK, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (boxSKS), radioMS, TRUE, TRUE, 2);
    gtk_container_add (GTK_CONTAINER (mbox), boxSKS);

    GtkLabel *board_label;
    board_label = (GtkLabel*)gtk_label_new("Wybierz rodzaj planszy");
    gtk_box_pack_start(GTK_BOX(data->server_box), (GtkWidget*)board_label, TRUE, TRUE, 0);

    GtkWidget *radioN, *radioE, *boxNE;
    boxNE = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_set_homogeneous (GTK_BOX (boxNE), TRUE);

    radioN = gtk_radio_button_new_with_label_from_widget (NULL, "Normalna");
    g_signal_connect (radioN, "clicked", G_CALLBACK (clicked_normal), data);
    radioE = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radioN),"Duża");
    g_signal_connect (radioE, "clicked", G_CALLBACK (clicked_hard), data);

    gtk_box_pack_start (GTK_BOX (boxNE), radioN, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (boxNE), radioE, TRUE, TRUE, 2);
    gtk_container_add (GTK_CONTAINER (data->server_box), boxNE);

    GtkLabel *set_label;
    set_label = (GtkLabel*)gtk_label_new("Wybierz rodzaj obrazkow");
    gtk_box_pack_start(GTK_BOX(data->server_box), (GtkWidget*)set_label, TRUE, TRUE, 0);

    GtkWidget *radioOSt, *radioOSw, *boxRO;
    boxRO = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_set_homogeneous (GTK_BOX (boxRO), TRUE);

    radioOSt = gtk_radio_button_new_with_label_from_widget (NULL, "Standardowy");
    g_signal_connect (radioOSt, "clicked", G_CALLBACK (clicked_icon_set0), data);
    radioOSw = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radioOSt),"Świateczny");
    g_signal_connect (radioOSw, "clicked", G_CALLBACK (clicked_icon_set1), data);

    gtk_box_pack_start (GTK_BOX (boxRO), radioOSt, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (boxRO), radioOSw, TRUE, TRUE, 2);
    gtk_container_add (GTK_CONTAINER (data->server_box), boxRO);

    gtk_container_add (GTK_CONTAINER (mbox), data->server_box);

    GtkWidget *play;
    play = gtk_button_new_with_label ("GRAJ");
    g_signal_connect (play, "clicked", G_CALLBACK (load_game), data->settings);
    gtk_container_add (GTK_CONTAINER (mbox), play);

    GtkWidget *ranking;
    ranking = gtk_button_new_with_label ("Ranking");
    g_signal_connect (ranking, "clicked", G_CALLBACK (create_ranking_window), NULL);
    gtk_container_add (GTK_CONTAINER (mbox), ranking);

    gtk_widget_show_all ((GtkWidget*)data->window);
}
