#include <stdbool.h>

typedef struct RankingData_type* RankingData;





RankingData create_RankingData_struct (void);
bool read_ranking (RankingData data);
int check_ranking_position  (int score, char mode);
void add_to_ranking(int score, const char name[20], char mode);
